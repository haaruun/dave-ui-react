import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Label, Input, Wrapper, Error,  Textarea} from './theme';

 export const TextArea = (props) => {
    const { label, value, type, changeFunc, error , name, ...other  } = props;    
    return ( 
        <Wrapper inputWrap>
        <Textarea {...other} name={name} type={type} required />
        <Label>{label}</Label>
        {(error !== null || error !== '') && <Error>{error}</Error>}
    </Wrapper>

    );
}

TextArea.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    changeFunc: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
};


TextArea.defaultProps = {
    error: '',
  };
     

