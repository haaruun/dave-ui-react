import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SearchWrapper, SearchInput, Container, Icon } from './theme';

 export const SearchField = (props) => {
    const { placeHolder, value, changeFunc, onClick, ...other} = props;    
    return ( 
        <SearchWrapper>
        <SearchInput {...other} placeholder={placeHolder} type="search" value={value} onChange={changeFunc} />
        <Container>
        <span className="highlight"></span>
       <span className="bar"></span>
       <span onClick={onClick} className="glyphicon glyphicon-search"></span>
       </Container>
      </SearchWrapper>    
    );
}


SearchField.propTypes = {
    placeHolder: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    changeFunc: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
};


