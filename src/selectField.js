import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Label, Input, Wrapper, Error, Select, SelectBar, SelectLabel } from './theme';

 export const SelectField = (props) => {
    const { label, value, changeFunc, error, optArray, name, language, ...other } = props;    
    return ( 
            <Wrapper selectWrap>
            <Select {...other} value={value} name={name} onChange={changeFunc}  required>
            
                {optArray.length > 0 &&
                    optArray.map(function(op, i) {
                    return (
                        <option value={op.id} key={i} required>
                        {op.text}
                        </option>

                    );
                    })}
                            </Select>
                            <SelectBar></SelectBar>
                            <Label>{label}</Label>
                            {(error !== null || error !== '') && <Error>{error}</Error>}
                            </Wrapper>
                    );
                }


SelectField.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    optArray: PropTypes.array.isRequired,
    changeFunc: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    error: PropTypes.string.isRequired,
};


SelectField.defaultProps = {
    error: '',
    optArray: []
  };


