import React, { Component } from 'react';
import { Text } from './theme';

class Toaster extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toasterSee: true
        };
        this.clickeventMan = () => this.setState({ toasterSee: false });
    }
    render() {
        const { toasterSee } = this.state;
        const { toastrText, clickFunc } = this.props;

        return (
            <div>
                {toasterSee ? (
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close-alert" onClick={this.clickeventMan}>
                            ✕
                        </button>

                        <Text H4>{toastrText}</Text>
                    </div>
                ) : (
                    ''
                )}
            </div>
        );
    }
}

export default Toaster;
