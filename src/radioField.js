import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, Radio, RadioLabel, Wrapper, Error, Check } from './theme';

export const RadioField = (props) => {
    const { id, name, value, label, changeFunc, checked, error, fontSize, scale, ...other} = props;    
    return ( 
        <Wrapper radio>
        <Radio
        {...other}
          type="radio"
          id={id}
          name={name}
          value={value}
          checked = {checked}
          onChange = {changeFunc}
          scale={scale}
        />
        <RadioLabel htmlFor={id}  scale={scale}>
          <Check />
          <Text P size={fontSize}>{label}</Text>
        </RadioLabel>
        {(error !== null || error !== '') && <Error>{error}</Error>}
      </Wrapper>
    );
}

RadioField.propTypes = {
    label: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    changeFunc: PropTypes.func.isRequired,
    checked: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
};


RadioField.defaultProps = {
    error: '',

  };