import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, Checkbox, CheckLabel, Wrapper, Error, Check } from './theme';

 export const CheckField = (props) => {
    const { disabled, id, name, value, label, changeFunc, checked, error, scale, fontSize, ...other  } = props;    
    return ( 
        <Wrapper checkbox>
        <Checkbox
          {...other}
          type="checkbox"
          id={id}
          name={name}
          value={value}
          checked = {checked}
          onChange = {disabled == true ? null : changeFunc}
          scale={scale}
          disabled={disabled}
          readOnly= {disabled == true ? true : false}
        />
        <CheckLabel htmlFor={id} scale={scale}>
          <Check />
          <Text P size={fontSize}>{label}</Text>
        </CheckLabel>
        {(error !== null || error !== '') && <Error>{error}</Error>}
      </Wrapper>
    );
}


CheckField.propTypes = {
    disarmed: PropTypes.bool,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    changeFunc: PropTypes.func.isRequired,
    checked: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
};


CheckField.defaultProps = {
    error: '',
  };