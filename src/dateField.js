import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Date, Error } from './theme';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

export const DateField = props => {
    const { label, error, dateValue, changeFunc, ...other } = props;
    return (
        <Date>
            <DatePicker {...other} />
            {(error !== null || error !== '') && <Error>{error}</Error>}
        </Date>
    );
};

DateField.propTypes = {
    error: PropTypes.string.isRequired
};

DateField.defaultProps = {
    error: ''
};
