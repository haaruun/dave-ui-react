export const VariableTheme = {
    color : {
        //Fixed
        bgColor: '#F5F6FA',
        secondBgColor: '#F1F2F6',
        black: '#041116',
        white: '#ffffff',
        //Customizable
        davePrimary: '#0099DA',
        daveDanger: '#E33A3A',
        daveTertiary: '#013243',
        daveSecondary: '#82D9FF',
        daveSuccess: '#3AE374',
        daveWarning: '#FFEE00',
    }, 

    font_size : {
        title: '30px',
        large: '22px',
        medium: '16px',
        small: '12px'

    },

    font_face : {
        fontPrimary: 'Montserrat',
        fontSecondary: 'Questrial'
    },

    img : {
        logo: 'https://image.ibb.co/cPCNb7/VWLogo.png',
        vehicle: 'https://image.ibb.co/ePwdYn/VWArten.png'
    },
    icons : {
        F_I: '',
        Claims: ''
    }

};