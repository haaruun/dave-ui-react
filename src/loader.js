import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Wrapper, Text} from './theme';

 export const Loader = (props) => {
    const {  title } = props;    
    return ( 
    <Wrapper loader>
    <div className='loader'>
        <div className='loader--dot'></div>
        <div className='loader--dot'></div>
        <div className='loader--dot'></div>
        <div className='loader--dot'></div>
        <div className='loader--dot'></div>
        <div className='loader--dot'></div>
        <Text loader content={title}></Text>
     </div>
</Wrapper>  
    );
}


Loader.propTypes = {
    title: PropTypes.string.isRequired
};


Loader.defaultProps = {
    title: 'Loading',
  };