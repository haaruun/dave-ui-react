import React, { Component } from 'react';
import styled, { css, keyframes, injectGlobal } from 'styled-components';
import { VariableTheme } from './variables';
import transition from 'styled-transition-group';

import { Link, NavLink } from 'react-router-dom';

export const Date = styled.div`
    position: relative;

    & > div > div {
        width: 100%;
    }

    & > div > div > div {
        width: 100%;
    }

    & > div > div > div > input {
        background: inherit;
        border: none;
        width: 100%;
        font-size: calc(16px + 0.25vw - 0.33em);
        font-family: Questrial;
        line-height: 1.3;
        border: 0;
        height: 30px;
        background-image: linear-gradient(#d2d2d2, #d2d2d2);
        background-size: 100% 2px;
        background-repeat: no-repeat;
        background-position: center bottom;
        background-color: inherit;
        color: black;
    }

    & > div > div > div > input::-webkit-input-placeholder {
        color: #9e9e9e;
    }

    & > div > div > div > input:focus {
        outline: none;
    }

    & > div > div > div > input:focus ~ .mama {
        font-size: 300px;
    }
`;

////////////////////////////////////// GLOBAL STYLES  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

injectGlobal`



.alert {
  min-width: 150px;
  padding: 15px !important;
  box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
  background: #0099DA;
  margin: 10px;
  position: fixed;
  bottom: 0;
  z-index: 1;
  width: 90%;
  
  span{
    padding: 0;
    margin: 0;
    color: white !important;

    &:before{
      content: "\f05a";
      font-family: FontAwesome;
      font-style: normal;
      font-weight: normal;
      text-decoration: inherit;
      padding: 10px;
    }
  }



  i{
    padding-right: 5px;
    vertical-align: middle;
    font-size: 24px;
  }
  .close-alert{
    background: red;
    -webkit-appearance: none;
    position: relative;
    float: right;
    padding: 0;
    border: 0;
    cursor: pointer;
    color: white;
    background: 0 0;
    font-size: 16px;
    line-height: 1;
    font-weight: bold;
    filter: alpha(opacity=40);
    &:hover{
      filter: alpha(opacity=70);
      opacity: .7;
    }
  }
}



input:-webkit-autofill {
  -webkit-box-shadow: 0 0 0 30px white inset !important;
  font-size: 14px !important;
  border-bottom: 2px solid #d2d2d2;
}

.Select.is-focused:not(.is-open) > .Select-control {
  border-color: #007eff;
  box-shadow: none !important;
  background: #fff;
}


.Select-option.is-focused {
  background-color: #0099DA !important;
  color: white !important;
  transition: 0.2s ease all;
}

.Select-option {
border-bottom: 0.5px solid rgba(0, 0, 0, 0.25);
}


.Select.is-disabled .Select-arrow-zone {
  opacity: 0 !important;
}

.Select.is-disabled .Select-value-label{
  color: #9e9e9e !important;
}

.Select-menu{
  background-color: white !important;
  font-family: Questrial !important;
  font-size: 14px !important;
  ::-webkit-scrollbar {
    width: 2px;
}
::-webkit-scrollbar-track {
    background-color: inherit;
}
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 0px;
    border-radius: 0px;
    background: lightgrey;
}
::-webkit-scrollbar-thumb:window-inactive {
    background: lightgrey;
}
}


.Select-control{
  color: #041116 !important;
  font-family: Questrial !important;
  font-size: calc(16px + 0.25vw - 0.33em) !important;
  font-family: Questrial !important;
  line-height: 1.3 !important;
  font-weight: normal !important;
  border: 0 !important;
  height: 30px !important;
  width: 100% !important;
  background-image: linear-gradient(#d2d2d2,#d2d2d2) !important;
  background-size: 100% 2px !important;
  background-repeat: no-repeat !important;
  background-position: center bottom !important;
  background-color: inherit !important;
  position: relative !important;
  width: 100% !important;
  border-radius: 0 !important;
  -webkit-appearance: none !important;
  -moz-appearance: none !important;
  appearance: none !important;
  -webkit-appearance: none !important;
  outline: none !important;
}


.Select-input{
  padding-left: 0px !important;
    padding-right: 0px !important;
  height: 30px !important;

}


.Select-placeholder,
.Select--single > .Select-control .Select-value {
 line-height: 28px !important;
  
}


.Select-placeholder, .Select--single > .Select-control .Select-value{
  padding-left: 0px !important;
    padding-right: 0px !important;
}


.Select is-clearable, .is-searchable, .Select--single{
  outline: none !important;
  box-shadow: none !important;
}

.Select-clear {
  font-size: 22px !important;
}


.__react_component_tooltip{
  background-color: ${VariableTheme.color.black} !important;
  
  &:after{
    border-right-color: ${VariableTheme.color.black} !important;
  }
  
  }
  
  
  
  .powerbi-frame {
    height: 550px;
  }


  .react-datepicker {
    font-family: Questrial;
    font-size: 0.8rem;
    background-color: #fff;
    color: #000;
    display: inline-block;
    position: relative;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    width: auto !important;
  }

  .available{
    background-color: ${VariableTheme.color.daveSuccess} !important;
    &:after{
      content: "⌘";
      font-size: 18px;
      color: white;
    }
  }

  .unavailable{
    background-color: ${VariableTheme.color.daveDanger} !important;
    &:after{
      content: "━";
      font-size: 16px;
      color: white;
    
    }
  }

  .pending{
    background-color: ${VariableTheme.color.daveWarning} !important;
    &:after{
      content: "\f067";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
    }
  }




/* DayPicker styles */

.DayPicker {
  display: inline-block;
}

.DayPicker-wrapper {
  position: relative;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  padding-bottom: 1rem;
  flex-direction: row;
}

.DayPicker-Months {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}

.DayPicker-Month {
  display: table;
  border-collapse: collapse;
  border-spacing: 0;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
  margin: 0 1rem;
  margin-top: 1rem;
}

.DayPicker-NavBar {
}

.DayPicker-NavButton {
  position: absolute;
  cursor: pointer;
  top: 1rem;
  right: 1.5rem;
  margin-top: 2px;
  color: #8b9898;
  width: 1.25rem;
  height: 1.25rem;
  display: inline-block;
  background-size: 50%;
  background-repeat: no-repeat;
  background-position: center;
}

.DayPicker-NavButton:hover {
  opacity: 0.8;
}

.DayPicker-NavButton--prev {
  margin-right: 1.5rem;
  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAwCAYAAAB5R9gVAAAABGdBTUEAALGPC/xhBQAAAVVJREFUWAnN2G0KgjAYwPHpGfRkaZeqvgQaK+hY3SUHrk1YzNLay/OiEFp92I+/Mp2F2Mh2lLISWnflFjzH263RQjzMZ19wgs73ez0o1WmtW+dgA01VxrE3p6l2GLsnBy1VYQOtVSEH/atCCgqpQgKKqYIOiq2CBkqtggLKqQIKgqgCBjpJ2Y5CdJ+zrT9A7HHSTA1dxUdHgzCqJIEwq0SDsKsEg6iqBIEoq/wEcVRZBXFV+QJxV5mBtlDFB5VjYTaGZ2sf4R9PM7U9ZU+lLuaetPP/5Die3ToO1+u+MKtHs06qODB2zBnI/jBd4MPQm1VkY79Tb18gB+C62FdBFsZR6yeIo1YQiLJWMIiqVjQIu1YSCLNWFgijVjYIuhYYCKoWKAiiFgoopxYaKLUWOii2FgkophYp6F3r42W5A9s9OcgNvva8xQaysKXlFytoqdYmQH6tF3toSUo0INq9AAAAAElFTkSuQmCC');
}

.DayPicker-NavButton--next {
  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAwCAYAAAB5R9gVAAAABGdBTUEAALGPC/xhBQAAAXRJREFUWAnN119ugjAcwPHWzJ1gnmxzB/BBE0n24m4xfNkTaOL7wOtsl3AXMMb+Vjaa1BG00N8fSEibPpAP3xAKKs2yjzTPH9RAjhEo9WzPr/Vm8zgE0+gXATAxxuxtqeJ9t5tIwv5AtQAApsfT6TPdbp+kUBcgVwvO51KqVhMkXKsVJFXrOkigVhCIs1Y4iKlWZxB1rX4gwlpRIIpa8SDkWmggrFq4IIRaJKCYWnSgnrXIQV1r8YD+1Vrn+bReagysIFfLABRt31v8oBu1xEBttfRbltmfjgEcWh9snUS2kNdBK6WN1vrOWxObWsz+fjxevsxmB1GQDfINWiev83nhaoiB/CoOU438oPrhXS0WpQ9xc1ZQWxWHqUYe0I0qrKCQKjygDlXIQV2r0IF6ViEBxVTBBSFUQQNhVYkHIVeJAtkNsbQ7c1LtzP6FsObhb2rCKv7NBIGoq4SDmKoEgTirXAcJVGkFSVVpgoSrXICGUMUH/QBZNSUy5XWUhwAAAABJRU5ErkJggg==');
}

.DayPicker-NavButton--interactionDisabled {
  display: none;
}

.DayPicker-Caption {
  padding: 0 0.5rem;
  display: table-caption;
  text-align: left;
  margin-bottom: 0.5rem;
}

.DayPicker-Caption > div {
  font-size: 1.15rem;
  font-weight: 500;
}

.DayPicker-Weekdays {
  margin-top: 1rem;
  display: table-header-group;
}

.DayPicker-WeekdaysRow {
  display: table-row;
}

.DayPicker-Weekday {
  display: table-cell;
  padding: 0.5rem;
  font-size: 0.875em;
  text-align: center;
  color: #8b9898;
}

.DayPicker-Weekday abbr[title] {
  border-bottom: none;
  text-decoration: none;
}

.DayPicker-Body {
  display: table-row-group;
}

.DayPicker-Week {
  display: table-row;
}

.DayPicker-Day {
  display: table-cell;
  padding: 0.5rem;
  text-align: center;
  cursor: pointer;
  vertical-align: middle;
}

.DayPicker-WeekNumber {
  display: table-cell;
  padding: 0.5rem;
  text-align: right;
  vertical-align: middle;
  min-width: 1rem;
  font-size: 0.75em;
  cursor: pointer;
  color: #8b9898;
  border-right: 1px solid #eaecec;
}

.DayPicker--interactionDisabled .DayPicker-Day {
  cursor: default;
}

.DayPicker-Footer {
  padding-top: 0.5rem;
}

.DayPicker-TodayButton {
  border: none;
  background-image: none;
  background-color: transparent;
  box-shadow: none;
  cursor: pointer;
  color: #4a90e2;
  font-size: 0.875em;
}

/* Default modifiers */

.DayPicker-Day--today {
  color: #d0021b;
  font-weight: 700;
}

.DayPicker-Day--outside {
  cursor: default;
  color: #8b9898;
}

.DayPicker-Day--disabled {
  color: #dce0e0;
  cursor: default;
  /* background-color: #eff1f1; */
}

/* Example modifiers */

.DayPicker-Day--sunday {
  background-color: #f7f8f8;
}

.DayPicker-Day--sunday:not(.DayPicker-Day--today) {
  color: #dce0e0;
}

.DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside) {
  position: relative;
  color: #f0f8ff;
  background-color: #4a90e2;
  border-radius: 100%;
}

.DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside):hover {
  background-color: #51a0fa;
}

.DayPicker:not(.DayPicker--interactionDisabled)
  .DayPicker-Day:not(.DayPicker-Day--disabled):not(.DayPicker-Day--selected):not(.DayPicker-Day--outside):hover {
  background-color: #f0f8ff;
  border-radius: 50%;
}

/* DayPickerInput */

.DayPickerInput {
  display: inline-block;
}

.DayPickerInput-OverlayWrapper {
  position: relative;
}

.DayPickerInput-Overlay {
  left: 0;
  z-index: 9999999;
  position: absolute;
  background: white;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.15);
  font-size: calc(16px + 0.25vw - 0.33em);
  font-family: Questrial;
}





@import url('https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');
@import url('https://fonts.googleapis.com/css?family=Questrial');
@import url("//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css");
@import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css');

html, body, div, span, applet, object, iframe,
  h1, h2, h3, h4, h5, h6, p, blockquote, pre,
  a, abbr, acronym, address, big, cite, code,
  del, dfn, em, img, ins, kbd, q, s, samp,
  small, strike, strong, sub, sup, tt, var,
  b, u, i, center,
  dl, dt, dd, ol, ul, li,
  fieldset, form, label, legend,
  table, caption, tbody, tfoot, thead, tr, th, td,
  article, aside, canvas, details, embed, 
  figure, figcaption, footer, header, hgroup, 
  menu, nav, output, ruby, section, summary,
  time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }


*,
*:before,
*:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}


body{
  overflow: hidden;
}


a{
  text-decoration: none !important;
}

input:required {
  box-shadow: none;
}

.loader {
  height: 20px;
  width: 250px;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
}
.loader--dot {
  animation-name: loader;
  animation-timing-function: ease-in-out;
  animation-duration: 3s;
  animation-iteration-count: infinite;
  height: 25px;
  width: 25px;
  border-radius: 100%;
  background-color: black;
  position: absolute;
}
.loader--dot:first-child {
  background-color: #0099DA;
  animation-delay: 0.5s;
}
.loader--dot:nth-child(2) {
  background-color: #E33A3A;
  animation-delay: 0.4s;
}
.loader--dot:nth-child(3) {
  background-color: #013243;
  animation-delay: 0.3s;
}
.loader--dot:nth-child(4) {
  background-color: #3AE374;
  animation-delay: 0.2s;
}
.loader--dot:nth-child(5) {
  background-color: #FFEE00;
  animation-delay: 0.1s;
}
.loader--dot:nth-child(6) {
  background-color: #041116;
  animation-delay: 0s;
}


@keyframes loader {
  15% {
    transform: translateX(0);
  }
  45% {
    transform: translateX(230px);
  }
  65% {
    transform: translateX(230px);
  }
  95% {
    transform: translateX(0);
  }
}



`;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ripple = keyframes`
5%, 100% {
  opacity: 0;
}
5% {
  opacity: 1;
}
`;

////////////////////////////////////// FOUNDATION COMPONENT(S)  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const langAnim = keyframes`
0% {
  opacity: 0;
}

100% {
  opacity: 1;
}
`;

export const Wrapper = styled.div`
background-color: inherit;
width: ${props => (props.span ? (props.span / 10) * 100 + '%' : 'auto')};


${props => props.radio && css``}

${props =>
    props.loader &&
    css`
        height: 100%;
        width: 100%;
        background: white;
        position: absolute;
        top: 0;
        left: 0;
        z-index: 999999;
        font-family: 'Questrial';
        color: white;
        font-size: 20px;
    `}






${props =>
    props.auto &&
    css`
        height: 100vh;
        min-height: 100%;
        width: 100vw;
        background-color: ${VariableTheme.color.bgColor};
        margin: 0;
        padding: 0;
        overflow-x: hidden;
        overflow-y: auto;
        ::-webkit-scrollbar {
            width: 2px;
        }
        ::-webkit-scrollbar-track {
            background-color: inherit;
        }
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 0px;
            border-radius: 0px;
            background: ${VariableTheme.color.davePrimary};
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: ${VariableTheme.color.davePrimary};
        }

        // @media (max-width: 1000px) {
        //   height: -webkit-fill-available;
        //   }
    `}

${({ center }) =>
    center &&
    `
margin: 0 auto;
`};



${props =>
    props.secTitleWrap &&
    css`
        border-bottom: 3px solid ${VariableTheme.color.black};
        width: auto;
        // margin: 0 auto;
        // margin-bottom: 5px;
        // padding-top: 0.5em;
        background-color: inherit;
        // margin: 15px;

        &:after {
            content: '';
            position: relative;
            border-bottom: 3px solid ${props => props.theme.primary || VariableTheme.color.davePrimary};
            width: 20%;
            width: ${props => (props.lineWidth / 10) * 100 + '%'};
            text-align: center;
            display: block;
            top: 3px;
        }
    `}


${props =>
    props.checkbox &&
    css`
        // margin: 20px 0 0 5px;
        // text-align: center;
    `}


${props =>
    props.checkcard &&
    css`
        display: inline-block;
        margin: 0;
        float: right;
    `}


${props =>
    props.table &&
    css`
        overflow-y: scroll;
        height: 600px;
        max-height: 650px;

        ::-webkit-scrollbar {
            width: 2px;
        }
        ::-webkit-scrollbar-track {
            background-color: inherit;
        }
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 0px;
            border-radius: 0px;
            background: ${VariableTheme.color.black};
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: ${VariableTheme.color.black};
        }
    `}






${props =>
    props.lang &&
    css`
position: relative;
width: 125px;
box-shadow: 0 1px 3px rgba(0,0,0,0.06), 0 1px 2px rgba(0,0,0,0.06);
animation: ${langAnim} 1s forwards;
   transition: 0.25s ease all;

&:after{
  position: absolute;
  top: 11.5px;
  right: 10px;
  width: 0;
  height: 0;
  padding: 0;
  content: '';
  border-left: 6px solid transparent;
  border-right: 6px solid transparent;
  border-top: 6px solid rgba(0, 0, 0, 0.12);
  pointer-even
}

@media (max-width: 800px) {
  right: 2.5vw;
}


`}


  ${props =>
      props.userLogo &&
      css`
          display: flex;
          align-items: center;
          padding-left: 3vw;
          position: relative;
          background: none;
          margin: auto;
          background: white;
          height: auto;
          width: auto;
      `}


${props =>
    props.remitwrap &&
    css`
        height: 100vh;
        max-height: calc(100vh - 169px);
        overflow-y: auto;
        width: 90%;
        margin: 0 auto;

        ::-webkit-scrollbar {
            width: 2px;
            height: 3px;
        }
        ::-webkit-scrollbar-track {
            background-color: inherit;
        }
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 0px;
            border-radius: 0px;
            width: 2px;
            background: #041116;
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: #041116;
        }
    `}


  ${props =>
      props.dealList &&
      css`
          height: 70vh;
          overflow-y: scroll;
          width: 90%;
          margin: 0 auto;
          background: inherit;

          ::-webkit-scrollbar {
              width: 2px;
          }
          ::-webkit-scrollbar-track {
              background-color: inherit;
          }
          ::-webkit-scrollbar-thumb {
              -webkit-border-radius: 0px;
              border-radius: 0px;
              background: #041116;
          }
          ::-webkit-scrollbar-thumb:window-inactive {
              background: #041116;
          }
      `}


${props =>
    props.productlist &&
    css`
        height: 70vh;
        overflow-y: scroll;
        width: 90%;
        margin: 0 auto;
        background: inherit;

        ::-webkit-scrollbar {
            width: 2px;
        }
        ::-webkit-scrollbar-track {
            background-color: inherit;
        }
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 0px;
            border-radius: 0px;
            background: #041116;
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: #041116;
        }

        &:before {
            content: '';
            border-left: 2px solid darkgrey;
            top: 8.5%;
            left: 0;
            position: absolute;
            height: 80%;
            width: 90%;
            display: none;
        }
    `}




  ${props =>
      props.inputWrap &&
      css`
          position: relative;
          width: auto;
          background-color: inherit;
      `}


  ${props =>
      props.selectWrap &&
      css`
  position: relative;
  // margin-top: 20px;
  //   margin-right: 10px;
  //   margin-left: 10px;
  //   margin-bottom: 20px;
    
  &:after{
  	position: absolute;
  	top: 7.5px;
  	right: 10px;
  	width: 0;
  	height: 0;
  	padding: 0;
  	content: '';
  	border-left: 6px solid transparent;
  	border-right: 6px solid transparent;
  	border-top: 6px solid rgba(0, 0, 0, 0.12);
    pointer-even
  }
  `}


  ${props =>
      props.rightPane &&
      css`
          position: sticky;
          top: 0;
          background: inherit;
          // height: 100vh;
          // overflow-y: scroll;

          ${props =>
              props.remittance &&
              css`
                  height: 100vh;
                  overflow-y: scroll;
                  background-color: white;
                  padding: 20px;

                  ::-webkit-scrollbar {
                      width: 2px;
                  }
                  ::-webkit-scrollbar-track {
                      background-color: inherit;
                  }
                  ::-webkit-scrollbar-thumb {
                      -webkit-border-radius: 0px;
                      border-radius: 0px;
                      background: #041116;
                  }
                  ::-webkit-scrollbar-thumb:window-inactive {
                      background: #041116;
                  }
              `};
      `}

  ${props =>
      props.nav &&
      css`
          height: auto;
          width: 100vw;
          position: relative;
          z-index: 1;
      `}

`;

export const Textanchor = styled.a`
    font-size: ${VariableTheme.font_size.medium};
    font-family: ${VariableTheme.font_face.fontSecondary};
    color: ${VariableTheme.color.black};
    text-decoration: none;
    display: inline-block;
    position: relative;
    padding-top: 2px;
    padding-bottom: 10px;
    width: auto;

    &:hover {
        color: ${props => props.theme.primary || VariableTheme.color.davePrimary};
        transition: 0.25s ease all;
    }

    ${props =>
        props.line &&
        css`
            &:after {
                content: '';
                position: relative;
                border-bottom: 2px solid ${props => props.theme.primary || VariableTheme.color.davePrimary};
                width: 0%;
                text-align: center;
                z-index: 9999;
                display: block;
                top: 12px;
                transition: 0.25s ease all;
            }

            &:hover:after {
                width: 100%;
                transition: 0.25s ease all;
            }
        `};
`;

export const Text = styled.span`
  display: block;
  font-size: 5px;
  color: pink;
  font-family: ${VariableTheme.font_face.fontSecondary};



  ${props =>
      props.loader &&
      css`
  font-size: calc(${VariableTheme.font_size.large} + 0.25vw - 0.33em);
  color: ${VariableTheme.color.black};
  font-family: ${VariableTheme.font_face.fontSecondary};
  position: absolute;
  top: 200%;
  left: 0;
  right: 0;
  width: 4rem;
  margin: auto;
  
&:after{
    content: '${props => props.content}';
    font-weight: bold;
}

`}


${props =>
    props.H1 &&
    css`
        font-size: calc(${VariableTheme.font_size.title} + 1vw - 0.5em);
        color: ${VariableTheme.color.black};
        font-family: ${VariableTheme.font_face.fontPrimary};
    `}

  ${props =>
      props.H2 &&
      css`
          font-size: calc(${VariableTheme.font_size.title} + 0.75vw - 0.75em);
          color: ${VariableTheme.color.black};
          font-family: ${VariableTheme.font_face.fontSecondary};
      `}

  ${props =>
      props.H3 &&
      css`
          font-size: calc(${VariableTheme.font_size.large} + 0.25vw - 0.33em);
          color: ${VariableTheme.color.black};
          font-family: ${VariableTheme.font_face.fontSecondary};
      `}

  ${props =>
      props.H4 &&
      css`
          font-size: calc(${VariableTheme.font_size.medium} + 0.25vw - 0.33em);
          color: ${VariableTheme.color.black};
          font-family: ${VariableTheme.font_face.fontSecondary};
      `}


  ${props =>
      props.P &&
      css`
          font-size: calc(${VariableTheme.font_size.small} + 0.25vw - 0.33em);
          font-size: calc(${props.size + 'px'} + 0.25vw - 0.33em);
          color: ${VariableTheme.color.black};
          font-family: ${VariableTheme.font_face.fontPrimary};
      `}


  ${props =>
      props.A &&
      css`
          font-size: calc(${VariableTheme.font_size.small} + 0.25vw - 0.33em);
          color: ${VariableTheme.color.black};
          font-family: ${VariableTheme.font_face.fontPrimary};
      `}
  
 

  ${props =>
      props.loginTitle &&
      css`
          font-family: Montserrat;
          font-size: calc(${VariableTheme.font_size.title} + 1vw - 0.5em);
          color: ${VariableTheme.color.black};
          margin-bottom: 0.75em;
          line-height: 1.25em;
          font-weight: 500;
      `}

  ${props =>
      props.loginSubTitle &&
      css`
          font-family: ${VariableTheme.font_face.fontSecondary};
          font-size: calc(${VariableTheme.font_size.title} + 0.5vw - 1em);
          color: ${props => props.theme.primary || VariableTheme.color.davePrimary};
          margin-bottom: 1.7em;
          line-height: 1.25em;
      `}


  ${props =>
      props.logoText &&
      css`
          font-size: ${VariableTheme.font_size.large};
          font-family: ${VariableTheme.font_face.fontSecondary};
          color: ${VariableTheme.color.black};
          padding-left: 5px;
      `}


  ${props =>
      props.sectionHeader &&
      css`
          font-size: calc(${VariableTheme.font_size.large} + 1vw - 0.95em);
          color: ${VariableTheme.color.black};
          font-family: ${VariableTheme.font_face.fontSecondary};
          border-bottom: 3px solid ${VariableTheme.color.black};
          width: 90%;
          margin: 0 auto;
          margin-bottom: 5px;
          padding-top: 1em;

          ${({ remittance }) =>
              remittance &&
              `
    display: table-cell;
    width: auto;
`};

          &:after {
              content: '';
              position: relative;
              border-bottom: 3px solid ${props => props.theme.primary || VariableTheme.color.davePrimary};
              width: 20%;
              text-align: center;
              z-index: 9999;
              display: block;
              top: 3px;
          }
      `}
`;

const logoAnim = keyframes`
0% {
  top: -5vh;
  opacity: 0;
}

100% {
  top: 5vh;
  opacity: 1;
}
`;

const carAnim = keyframes`
0% {
  opacity: 0;
  top: calc(35% - 2vw);
}

100% {
  opacity: 1;
  top: calc(33% - 2vw);
}
`;

export const Image = styled.img`

${props =>
    props.loginCar &&
    css`
        height: auto;
        width: 90%;
        min-width: 400px;
        display: block;
        margin: 0 auto;
        position: relative;
        animation: ${carAnim} 1s forwards;
        transition: 0.5s ease all;

        @media (max-width: 1000px) {
            display: none;
        }
    `}

  ${props =>
      props.logo &&
      css`
          width: 200px;
          min-width: 30px;
          height: auto;
      `}


  ${props =>
      props.loginLogo &&
      css`
          height: auto;
          width: calc(225px + 1vw - 1em);
          min-width: 30px;
          animation: ${logoAnim} 1s forwards;
          transition: 0.5s ease all;
      `}

`;

////////////////////////////////////// GRID COMPONENT(S)  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const Container = styled.div`
    margin: 0 auto;

    ${({ visibile }) =>
        visibile &&
        `
display: none;
margin: 0;
`};
`;

export const Row = styled.div`
  display: flex;
  flex-flow: row wrap;
  height: auto;
  min-height: inherit;
  height:  ${props => props.viewHeight}vh;
  
  & > div{
    padding: 10px;
    padding: ${props => props.gutter}px;

  }
  
 

  ${props =>
      props.title &&
      css`
          display: flex;
          justify-content: space-between;
          align-items: baseline;
          width: 100%;
          margin: 0 auto;
          background-color: inherit;
          height: auto;
          align-items: center;
          background-color: inherit;
      `}


  ${props =>
      props.line &&
      css`
          border-bottom: 2px solid black;
      `}





${({ justifycenter }) =>
    justifycenter &&
    `
justify-content: center;
`};


${({ justifyend }) =>
    justifyend &&
    `
justify-content: flex-end;
`};


${({ justifybetween }) =>
    justifybetween &&
    `
justify-content: space-between;
`};

${({ aligncenter }) =>
    aligncenter &&
    `
align-items: center;
`};


`;

const getFlex = span => {
    if (!span) return;

    let flex = (span / 12) * 100;
    return `flex-basis: ${flex}%;`;
};

export const FlexItem = styled.div`
  min-height: auto;
  // min-width: ${props => (props.nav && 'auto') || '150px'};

  &:empty{
padding: 10px;
  }

  &:empty:after{
    content: 'TBD';
    background-color: white;
    height: 40vh;
    width: 100%;
    text-align: center;
    line-height: 40vh;
    display: block;
    position: relative;
    font-size: calc(${VariableTheme.font_size.title} + 0.25vw - 0.33em);
    color: ${VariableTheme.color.black};
    font-family: ${VariableTheme.font_face.fontSecondary};
    box-shadow: 0 1px 1px rgba(0,0,0,0.025), 0 1px 1px rgba(0,0,0,0.025);

}
  }




  ${({ textAlignCenter }) =>
      textAlignCenter &&
      `
   text-align: center;
}
`};


${({ alignCenter }) =>
    alignCenter &&
    `
  margin: 0 auto;
}
`};


${({ textAlignRight }) =>
    textAlignRight &&
    `
text-align: right;
}
`};






${props =>
    props.pane &&
    css`
        background-color: ${props => props.bg};
        height: inherit;
        position: relative;
    `}





  ${({ xs }) => (xs ? getFlex(xs) : 'flex-basis: 100%')};
  
  @media (max-width: 1300px) {
      ${({ xl }) => xl && getFlex(xl)};
    }

    @media (max-width: 1000px) {
      ${({ lg }) => lg && getFlex(lg)};
      display:  ${props => props.display};
    }

    @media (max-width: 780px) {
      ${({ md }) => md && getFlex(md)};
    }
   
    @media (max-width: 600px) {
      ${({ sm }) => sm && getFlex(sm)};
    }

    @media (max-width: 450px) {
      ${({ xsm }) => xsm && getFlex(xsm)};
      order: 2;
    }

`;

////////////////////////////////////// BUTTON COMPONENT  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const supportBtnAnim = keyframes`
0% {
  opacity: 0;
  right: 0%;
}

100% {
  opacity: 1;
  right: 2.5%;
}
`;

export const Arrow = styled.div`
    position: relative;
    display: inline-block;
    color: black;
    text-align: center;
    cursor: pointer;
    transform: rotate(0deg);

    &:before {
        content: '\f078';
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
    }

    ${({ close }) =>
        close &&
        `
transform: rotate(-90deg);
`};
`;

export const Button = styled.button`
     background: ${VariableTheme.color.black};
     border: 2px solid ${VariableTheme.color.black};
     color: white;
     text-align: center;
     text-decoration: none;
     font-size: ${props => (props.LoginBtn && 'calc(16px + 0.33vw - 0.5em)') || 'calc(12px + 0.33vw - 0.33em)'};
     width: auto;
     min-width: 75px;
     padding: 6px 12px;
     white-space: nowrap;
     margin-top: 10px;
     margin-right: 5px;
     height: 35px;
     display: ${props => (props.LoginBtn && 'block') || 'inline-block'};
     border-radius: 30px;
     font-family: ${VariableTheme.font_face.fontSecondary};  
     transition: 0.2s ease all;
     cursor: pointer;
     outline: none !important;

  
  
${props =>
    props.primary &&
    css`
        background: ${props.theme.primary || VariableTheme.color.davePrimary};
        border: 2px solid ${props.theme.primary || VariableTheme.color.davePrimary};
    `}
  ${props =>
      props.success &&
      css`
          background: ${props.theme.success || VariableTheme.color.daveSuccess};
          border: 2px solid ${props.theme.success || VariableTheme.color.daveSuccess};
      `}
  ${props =>
      props.tertiary &&
      css`
          background: ${props.theme.tertiary || VariableTheme.color.daveTertiary};
          border: 2px solid ${props.theme.tertiary || VariableTheme.color.daveTertiary};
      `}
  ${props =>
      props.danger &&
      css`
          background: ${props.theme.danger || VariableTheme.color.daveDanger};
          border: 2px solid ${props.theme.danger || VariableTheme.color.daveDanger};
      `}
  ${props =>
      props.secondary &&
      css`
          background: ${props.theme.secondary || VariableTheme.color.daveSecondary};
          border: 2px solid ${props.theme.secondary || VariableTheme.color.daveSecondary};
      `}
  ${props =>
      props.warning &&
      css`
          background: ${props.theme.warning || VariableTheme.color.daveWarning};
          border: 2px solid ${props.theme.warning || VariableTheme.color.daveWarning};
      `}

  ${props =>
      props.roundSlide &&
      css`
          font-size: calc(${VariableTheme.font_size.medium} + 0.25vw - 0.33em);
          position: ${props => (props.login && 'absolute') || 'relative'};
          width: auto;
          min-width: 40px;
          max-width: 40px;
          height: 40px;
          border-radius: 50px;
          padding-left: 12px;
          display: block;
          white-space: nowrap;
          box-sizing: border-box;
          color: white;
          text-decoration: none;
          overflow: hidden;
          text-align: center;
          transition: 0.4s ease all;
          margin-top: 0px;
          margin-right: 0px;

          ${props =>
              props.absolute &&
              css`
                  animation: ${supportBtnAnim} 1s forwards;
                  bottom: 2.5%;
                  right: 2.5%;
                  position: absolute;
              `} span {
              margin-right: 20px;
              transition: margin-right 0.2s ease;
          }
          &:hover {
              width: auto;
              min-width: 100px;
              max-width: 200px;
              padding-left: 7px;
              transition: 0.4s ease all !important;
              span {
                  margin-right: 10px;
              }
          }
      `}



${props =>
    props.round &&
    css`
        border: 0.5px solid ${VariableTheme.color.black};
        color: white;
        text-align: center;
        text-decoration: none;
        margin-right: 0px;
        display: inline-block;
        font-family: ${VariableTheme.font_face.fontSecondary};
        transition: 0.2s ease all;
        display: inline-block;
        height: 25px;
        width: 25px;
        min-width: 25px;
        background: red;
        border-radius: 50%;
        text-align: center;
        padding: 0px 0px;
        font-size: calc(${VariableTheme.font_size.small} + 0.25vw - 0.33em);
        margin-top: 0px;
    `}




${props =>
    props.product &&
    css`
        border: 0.5px solid ${VariableTheme.color.black};
        color: white;
        text-align: center;
        text-decoration: none;
        margin-right: 0px;
        display: ${props => (props.LoginBtn && 'block') || 'inline-block'};
        font-family: ${VariableTheme.font_face.fontSecondary};
        transition: 0.2s ease all;
        display: inline-block;
        height: 25px;
        width: 25px;
        min-width: 25px;
        background: purple;
        border-radius: 50%;
        text-align: center;
        line-height: 25px;
        font-size: calc(${VariableTheme.font_size.small} + 0.25vw - 0.33em);
        color: white;
        position: relative;
        left: -20px;
        margin-top: 0px;
        padding: 0px 0px;
        float: left;
        bottom: 5px;
    `}





  &:hover {
    background-color: white;
    cursor: pointer;
    transition: 0.2s ease all;
    color: ${VariableTheme.color.black};
    
  ${props =>
      props.primary &&
      css`
          background: white;
          border: 2px solid ${props.theme.primary || VariableTheme.color.davePrimary};
          color: ${props.theme.primary || VariableTheme.color.davePrimary};
      `}
${props =>
    props.success &&
    css`
        background: ${VariableTheme.color.bgColor};
        border: 2px solid ${props.theme.success || VariableTheme.color.daveSuccess};
        color: ${props.theme.success || VariableTheme.color.daveSuccess};
    `}
${props =>
    props.tertiary &&
    css`
        background: ${VariableTheme.color.bgColor};
        border: 2px solid ${props.theme.tertiary || VariableTheme.color.daveTertiary};
        color: ${props.theme.tertiary || VariableTheme.color.daveTertiary};
    `}
${props =>
    props.danger &&
    css`
        background: ${VariableTheme.color.bgColor};
        border: 2px solid ${props.theme.danger || VariableTheme.color.daveDanger};
        color: ${props.theme.danger || VariableTheme.color.daveDanger};
    `}
${props =>
    props.secondary &&
    css`
        background: ${VariableTheme.color.bgColor};
        border: 2px solid ${props.theme.secondary || VariableTheme.color.daveSecondary};
        color: ${props.theme.secondary || VariableTheme.color.daveSecondary};
    `}
${props =>
    props.warning &&
    css`
        background: ${VariableTheme.color.bgColor};
        border: 2px solid ${props.theme.warning || VariableTheme.color.daveWarning};
        color: ${props.theme.warning || VariableTheme.color.daveWarning};
    `}

${props =>
    props.search &&
    css`
        background-color: inherit;
        border: 2px solid ${props.theme.primary || VariableTheme.color.primary};
        color: ${props.theme.primary || VariableTheme.color.davePrimary};
    `}

}
`;

//////////////////////////////////////  FORM COMPONENT(S)  (+ DATEPICKER) ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const logFormAnim = keyframes`
0% {
  opacity: 0;
  top: -10px;
  
}

100% {
  opacity: 1;
  top: 0px;
}
`;

export const Form = styled.form`
    // height: auto;
    //     width: auto;
    //     margin: 15px;

    ${props =>
        props.loginFormWrap &&
        css`
            width: 70%;
            animation: ${logFormAnim} 1s forwards;
            transition: 0.5s ease all;
            position: relative;
            margin: 0 auto;
        `};
`;

const inputAnim = keyframes`
0% {
  background-size: 0% 2px, 100% 2px;
}

100% {
  background-size: 100% 2px, 100% 2px;
}
`;

export const Input = styled.input`
    font-size: calc(${VariableTheme.font_size.medium} + 0.25vw - 0.33em);
    font-family: ${VariableTheme.font_face.fontSecondary};
    line-height: 1.3;
    border: 0;
    height: 30px;
    width: 100%;
    background-image: linear-gradient(#d2d2d2, #d2d2d2);
    background-size: 100% 2px;
    background-repeat: no-repeat;
    background-position: center bottom;
    background-color: inherit;
    color: #9e9e9e;

    &:focus,
    &:valid {
        color: ${VariableTheme.color.black};
    }

    &:-webkit-autofill ~ label {
        top: -10px;
        font-size: 80%;
        color: #0099da;
    }

    // &:empty ~ label{
    //   color: red;
    // }

    // &[type="email"]:empty ~ label{
    //   color: red;
    // }

    // &:not([value=""]):not(:focus):invalid~ label {
    //   color: yellow !important;
    // }

    &:focus ~ label,
    &:valid ~ label,
    &:read-only ~ label,
    &:disabled ~ label {
        top: -10px;
        font-size: 80%;
        color: ${props => props.theme.primary || VariableTheme.color.davePrimary};
    }

    &:focus {
        background-image: linear-gradient(${props => props.theme.primary || VariableTheme.color.davePrimary}, ${props => props.theme.primary || VariableTheme.color.davePrimary}),
            linear-gradient(#d2d2d2, #d2d2d2);
        animation: ${inputAnim} 0.5s forwards;
        box-shadow: none;
        outline: 0;
        background-size: 0% 2px;
    }
`;

export const Textarea = styled.textarea`
    font-size: calc(${VariableTheme.font_size.medium} + 0.25vw - 0.33em);
    font-family: ${VariableTheme.font_face.fontSecondary};
    line-height: 1.3;
    border: 0;
    height: 30px;
    width: 100%;
    background-image: linear-gradient(#d2d2d2, #d2d2d2);
    background-size: 100% 2px;
    background-repeat: no-repeat;
    background-position: center bottom;
    background-color: inherit;
    color: ${VariableTheme.color.black};
    resize: none;
    transition: 0.2s ease all;
    background-color: inherit;

    &:placeholder-shown ~ label {
        top: -10px;
        font-size: 80%;
        color: #0099da;
    }

    &:focus ~ label,
    &:valid ~ label,
    &:read-only ~ label,
    &:disabled ~ label {
        top: -10px;
        font-size: 80%;
        color: ${props => props.theme.primary || VariableTheme.color.davePrimary};
    }

    &:focus {
        background-image: linear-gradient(${props => props.theme.primary || VariableTheme.color.davePrimary}, ${props => props.theme.primary || VariableTheme.color.davePrimary}),
            linear-gradient(#d2d2d2, #d2d2d2);
        animation: ${inputAnim} 0.5s forwards;
        box-shadow: none;
        outline: 0;
        background-size: 0% 2px;
        height: 60px;
        transition: 0.2s ease all;
    }

    &:focus,
    &:valid {
        height: 60px;
        transition: 0.2s ease all;
    }

    //  &:invalid{
    //    background-color: red;

    //  }
`;

export const Label = styled.label`
color: #9e9e9e;  
font-family: ${VariableTheme.font_face.fontSecondary}  
font-size: calc(${VariableTheme.font_size.medium} + 0.25vw - 0.33em);
font-weight: normal;
position: absolute;
pointer-events: none;
left: 0px;
top: 5px;
transition: 0.2s ease all;
`;

export const Select = styled.select`
    color: ${VariableTheme.color.black};
    font-family: Questrial;
    font-size: calc(${VariableTheme.font_size.medium} + 0.25vw - 0.33em);
    font-family: ${VariableTheme.font_face.fontSecondary};
    padding-bottom: 6px;
    line-height: 1.3;
    font-weight: normal;
    border: 0;
    height: 30px;
    width: 100%;
    background-image: linear-gradient(#d2d2d2, #d2d2d2);
    background-size: 100% 2px;
    background-repeat: no-repeat;
    background-position: center bottom;
    background-color: inherit;
    position: relative;
    width: 100%;
    border-radius: 0;
    appearance: none;
    -webkit-appearance: none;
    outline: none;

    &:focus ~ label,
    &:valid ~ label,
    &:read-only ~ label,
    &:disabled ~ label {
        color: ${props => props.theme.primary || VariableTheme.color.davePrimary};
        top: -12.5px;
        transition: 0.2s ease all;
        font-size: 14px;
    }

    &:focus {
        outline: none;
        border-bottom: 1px solid rgba(0, 0, 0, 0);
        color: ${props => props.theme.primary || VariableTheme.color.davePrimary};
    }

    &:focus ~ span:before,
    &:focus ~ span:after {
        width: 50%;
    }

    ${props =>
        props.langSelect &&
        css`
            padding-left: 15px;
            padding-top: 5px;
            background-image: linear-gradient(white, white);
            background-color: white;

            &:focus {
                border-bottom: 0px solid rgba(0, 0, 0, 0);
            }

            &:focus ~ span:before,
            &:focus ~ span:after {
                width: 0%;
            }
        `};
`;

export const SelectBar = styled.span`
    position: relative;
    display: block;
    width: 100%;

    &:before,
    &:after {
        content: '';
        height: 2px;
        width: 0;
        bottom: 1px;
        position: absolute;
        background: ${props => props.theme.primary || VariableTheme.color.davePrimary};
        transition: 0.2s ease all;
    }

    &:before {
        left: 50%;
    }

    &:after {
        right: 50%;
    }
`;

export const SelectLabel = styled.label`
color: #9e9e9e;  
font-family: ${VariableTheme.font_face.fontSecondary}  
font-size: calc(${VariableTheme.font_size.medium} + 0.25vw - 0.33em);
font-weight: normal;
position: absolute;
pointer-events: none;
left: 0px;
top: 5px;
transition: 0.2s ease all;

`;

export const Error = styled.p`
    font-size: calc(10px + 0.33vw - 0.25em);
    color: ${props => props.theme.danger || VariableTheme.color.daveDanger};
    font-family: ${VariableTheme.font_face.fontPrimary};
`;

export const Check = styled.span``;

export const Radio = styled.input`
    display: none;

    &:disabled + label > span:nth-of-type(1) {
        border: 2px solid lightgrey;
    }

    &:disabled + label > span:nth-of-type(1):hover {
        cursor: not-allowed;
    }

    &:disabled + label > span:nth-of-type(2) {
        color: lightgrey;
    }

    &:disabled + label > span:nth-of-type(2):hover {
        cursor: not-allowed;
    }

    &:checked + label > span:nth-of-type(1) {
        background-color: white;
        border: 1px solid ${VariableTheme.color.davePrimary};
        transform: scale(1.1);
        transform: scale(${props => props.scale / 10 + 0.05});
    }

    &:checked + label > span:nth-of-type(1):after {
        background: ${VariableTheme.color.davePrimary};
        transition: width 150ms ease 100ms;
    }

    &:checked + label > span:nth-of-type(1):before {
        background: ${VariableTheme.color.davePrimary};
        transition: width 150ms ease 100ms;
    }
`;

export const Checkbox = styled.input`
    display: none;

    &:disabled + label > span:nth-of-type(1) {
        border: 2px solid lightgrey;
    }

    &:disabled + label > span:nth-of-type(1):after {
        ${props => props.checked ? '' : 'width: 0px !important' }
    }

    &:disabled + label > span:nth-of-type(1):before {
        ${props => props.checked ? '' : 'width: 0px !important' }
    }

    &:disabled + label > span:nth-of-type(1):hover {
        cursor: not-allowed;

    }

    &:disabled + label > span:nth-of-type(2) {
        color: lightgrey;
        
    }

    &:disabled + label > span:nth-of-type(2):hover {
        cursor: not-allowed;
        
    }

    &:checked + label > span:nth-of-type(1) { // still this
        background-color: white;
        border: 1px solid ${VariableTheme.color.davePrimary};
        transform: scale(1.05);
        transform: scale(${props => props.scale / 10 + 0.05});
    }

    &:checked + label > span:nth-of-type(1):after {
        width: 10px;
        background: ${VariableTheme.color.davePrimary};
        transition: width 150ms ease 100ms;
    }

    &:checked + label > span:nth-of-type(1):before {
        width: 5px;
        background: ${VariableTheme.color.davePrimary};
        transition: width 150ms ease 100ms;
    }

    &:checked + label:hover > span:nth-of-type(1) {
        background-color: white;
        border: 1px solid ${VariableTheme.color.davePrimary};
        transform: scale(1.05);
        transform: scale(${props => props.scale / 10 + 0.05});
    }
    &:checked + label:hover > span:nth-of-type(1):after {
        width: 10px;
        background: ${VariableTheme.color.davePrimary};
        transition: width 150ms ease 100ms;
    }
    &:checked + label:hover > span:nth-of-type(1):before {
        width: 5px;
        background: ${VariableTheme.color.davePrimary};
        transition: width 150ms ease 100ms;
    }
`;

export const RadioLabel = styled.label`
    display: inline-block;
    color: black;
    cursor: pointer;
    position: relative;
    top: 0px !important;
    text-align: center;

    & > span:nth-of-type(1) {
        display: inline-block;
        position: relative;
        background-color: white;
        width: 25px;
        height: 25px;
        transform-origin: center;
        border: 0.5px solid black;
        border-radius: 50%;
        vertical-align: -8px;
        margin-right: 0.5em;
        transition: background-color 150ms 200ms, transform 350ms cubic-bezier(0.78, -1.22, 0.17, 1.89);
        transform: scale(${props => props.scale / 10});
    }
    & > span:nth-of-type(1):before {
        content: '';
        width: 0px;
        transition: width 50ms ease 50ms;
        transform-origin: 0% 0%;
        position: relative;
        display: block;
        height: 15px;
        width: 15px;
        top: 4px;
        border-radius: 50%;
        margin: 0 auto;
    }

    & > span:nth-of-type(2) {
        display: inline-block;
    }
`;

export const CheckLabel = styled.label`
    display: inline-block;
    color: black;
    cursor: pointer;
    position: relative;
    top: 0px !important;
    text-align: center;

    & > span:nth-of-type(1) {
        display: inline-block;
        position: relative;
        background-color: white;
        width: 25px;
        height: 25px;
        transform-origin: center;
        border: 0.5px solid black;
        border-radius: 0%;
        vertical-align: -8px;
        margin-right: 0.5em;
        transition: background-color 150ms 200ms, transform 350ms cubic-bezier(0.78, -1.22, 0.17, 1.89);
        transform: scale(${props => props.scale / 10});
    }
    & > span:nth-of-type(1):before {
        content: '';
        width: 0px;
        height: 2px;
        border-radius: 2px;
        background: rgba(0, 0, 0, 0.6);
        position: absolute;
        transform: rotate(45deg);
        top: 11px;
        left: 8px;
        transition: width 50ms ease 50ms;
        transform-origin: 0% 0%;
    }
    & > span:nth-of-type(1):after {
        content: '';
        width: 0;
        height: 2px;
        border-radius: 2px;
        background: rgba(0, 0, 0, 0.6);
        position: absolute;
        transform: rotate(305deg);
        top: 15px;
        left: 9px;
        transition: width 50ms ease;
        transform-origin: 0% 0%;
    }
    &:hover span:nth-of-type(1):before {
        width: 5px;
        transition: width 100ms ease;
    }
    &:hover span:nth-of-type(1):after {
        width: 10px;
        transition: width 150ms ease 100ms;
    }

    & > span:nth-of-type(2) {
        display: inline-block;
    }
`;

////////////////////////////////////// CARD COMPONENT  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const Card = styled.div`
    height: auto;
    width: auto;
    background-color: white;
    font-family: ${VariableTheme.font_face.fontSecondary};
    font-size: ${VariableTheme.font_size.medium};
    color: black;
    transition: 0.2s ease all;
    padding: 20px;

    ${props =>
        props.auto &&
        css`
            height: auto;
            width: auto;
            text-align: center;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.075), 0 1px 2px rgba(0, 0, 0, 0.075);

            &:empty {
            }
        `};

    ${props =>
        props.list &&
        css`
            width: 98%;
            text-align: center;
            margin-bottom: 1.5px;
            background-color: white;

            &:hover {
                background-color: ${props => props.theme.secondary || VariableTheme.color.daveSecondary};
                transition: 0.2s ease all;
                color: white;
                cursor: pointer;
            }
        `} ${({ title }) =>
        title &&
        `
    display: inline-block;
    margin-top: 1.5em;
`};
`;

////////////////////////////////////// NAV COMPONENT  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const TopNav = styled.div`
    width: 100%;
    height: 56px;
    background-color: white;
    margin: 0;
    padding: 0;
    display: flex;
    align-items: center;
    flex-flow: row wrap;
    border-bottom: 1px solid rgba(0, 0, 0, 0.05);
`;

export const BottomNav = styled.div`
    width: 100%;
    height: 44px;
    background-color: white;
    margin: 0;
    padding: 0;
    display: flex;
    align-items: center;
    flex-flow: row wrap;
    &:after {
        bottom: -5px;
        box-shadow: inset 0px 4px 8px -3px rgba(17, 17, 17, 0.06);
        content: '';
        height: 5px;
        left: 0px;
        opacity: 1;
        pointer-events: none;
        position: absolute;
        right: 0px;
        width: 100%;
        z-index: 2050;
    }
`;

export const NavTitleWrap = styled.div`
    flex: 1;
    flex-basis: 25%;
    text-align: center;
    align-items: center;
`;

export const NavTitle = styled.h2`
    font-size: ${VariableTheme.font_size.large};
    color: ${VariableTheme.color.black};
    font-family: ${VariableTheme.font_face.fontSecondary};

    @media (max-width: 800px) {
        display: none;
    }
`;

export const NavElemWrap = styled.ul`
    flex: 1;
    flex-basis: 70%;
    display: flex;
    align-items: center;
    list-style-type: none;
    height: 100%;

    @media (max-width: 800px) {
        flex-basis: 100%;
    }
`;

export const NavItem = styled.li`
  text-align: center;
  flex: 1;
  height: 100%;
  }

`;

export const StyledLink = styled(NavLink)`
font-size: calc(${VariableTheme.font_size.medium} + 0.5vw - 0.5em);
text-align: center;
color: ${VariableTheme.color.black};
font-family: ${VariableTheme.font_face.fontPrimary};
transition: 0.2s ease all;
text-decoration: none !important;
height: 100%;
    display: block;
    line-height: 44px;
    width: 50%;
    margin: 0 auto;

    &:after{
      content:  '${props => props.content}';
    }

&:hover {
      color: ${VariableTheme.color.davePrimary};
      transition: 0.2s ease all;
    }
${props =>
    props.active &&
    css`
        color: ${props.theme.primary || VariableTheme.color.davePrimary} !important;
        border-bottom: 3px solid ${props.theme.primary || VariableTheme.color.davePrimary};
    `}


  @media (max-width: 700px){
    &:after{
      content: '${props => props.icon}';
      font-family: FontAwesome;
      font-style: normal;
      font-weight: normal;
      text-decoration: inherit;
      font-size: calc(${VariableTheme.font_size.medium} + 0.5vw - 0.25em);
    }
`;

export const MobileSearchWrapper = styled.div`
@media (min-width: 800px){
  display: none;
}
position: relative;
padding: 1rem 0 0.5rem 0;
height: auto;
display: none;
color: #9e9e9e;  
font-family: ${VariableTheme.font_face.fontSecondary}  
font-weight: normal;
padding: 15px;

& > span:nth-of-type(3) {
  position: absolute;
  color: black;
  left: 92%;
  top: 1.625rem;
  font-size: calc(${VariableTheme.font_size.medium} + 0.5vw - 0.25em);
  transition: 0.2s ease all;
}

& > input:focus ~ span:nth-of-type(3) {
  left: 94%;
  top: 1.625rem;
  color: ${VariableTheme.color.black};
  transition: 0.25s ease all;
} 


& > span:nth-of-type(2) {
  display: block;
  width: 0;
  margin-left: 50%;
  height: 1px;
  background: black;
  opacity: 0;
  transition: 0.2s ease all;
  -moz-transition: 0.2s ease all;
  -webkit-transition: 0.2s ease all;
}

& > input:focus ~ span:nth-of-type(2) {
  opacity: 1;
  width: 100%;
  margin-left: 0;
}
}


${props =>
    props.wide &&
    css`
        width: 100%;
        background-color: white;
        display: block;
    `}


`;

export const SearchWrapper = styled.div`
position: relative;
height: auto;
width: 100%;
color: #9e9e9e;  
font-family: ${VariableTheme.font_face.fontSecondary}  
font-weight: normal;

& > div > span:nth-of-type(3) {
  position: absolute;
  color: black;
  right: 5px;
  top: 8px;
  font-size: calc(${VariableTheme.font_size.medium} + 0.33vw - 0.33em);
  transition: 0.2s ease all;
}

& > input:focus ~ div > span:nth-of-type(3),
& > input:valid ~ div > span:nth-of-type(3){
  right: 0px;
  color: ${VariableTheme.color.black};
  transition: 0.25s ease all;
  cursor: pointer;
} 


& > div > span:nth-of-type(2) {
  display: block;
  width: 0;
  margin-left: 50%;
  height: 1px;
  background: black;
  opacity: 0;
  transition: 0.2s ease all;
  -moz-transition: 0.2s ease all;
  -webkit-transition: 0.2s ease all;
}

& > input:focus ~ div > span:nth-of-type(2) {
  opacity: 1;
  width: 100%;
  margin-left: 0;
}
}



${props =>
    props.desk &&
    css`
        @media (max-width: 800px) {
            display: none;
        }
    `}





${props =>
    props.model &&
    css`
        width: 100%;
        margin: 0 auto;
        display: flex;
        padding: 0;
        margin-bottom: 10px;

        & > input:empty ~ span {
            opacity: 1;
        }
    `}


`;

export const SearchInput = styled.input`
    border: none;
    border-bottom: 1px solid black;
    display: block;
    font-size: calc(${VariableTheme.font_size.medium} + 0.33vw - 0.5em);
    line-height: 1rem;
    margin: 0;
    padding: 0.5rem 0;
    width: 100%;
    text-align: left;
    color: black;
    outline: none;
    background-color: inherit;

    ${props =>
        props.model &&
        css`
            border: none;
            border-bottom: 0px;
            display: block;
            line-height: 1rem;
            margin: 0;
            padding: 0.5rem 0;
            width: 100%;
            text-align: left;
            color: black;
            outline: none;
            background-color: white;
            background: white;
            height: 30px;
            border-radius: 50px;
            padding: 15px;
            margin-bottom: 10px;
            width: 100%;
            margin: 0 auto;
        `};
`;

////////////////////////////////////// CHART COMPONENT  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////// DROPDOWN COMPONENT  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const DropUser = styled.div`
    font-family: ${VariableTheme.font_face.fontPrimary};
    font-size: calc(${VariableTheme.font_size.medium} + 0.25vw - 0.4em);
    color: ${VariableTheme.color.black};
    display: inline-block;
    padding: 15px;
    position: relative;

    & > ul {
        display: none;
    }

    &:hover {
        cursor: pointer;
        background-color: ${VariableTheme.color.bgColor};
    }

    &:hover ul {
        display: block;
        margin-top: 15px;
        width: 100%;
        height: 30px;
        line-height: 30px;
        left: 0;
        position: absolute;
    }

    &:hover ul li {
        display: block;
        background-color: ${VariableTheme.color.secondBgColor};
    }

    &:hover ul li:hover {
        background-color: #${VariableTheme.color.bgColor};
    }
`;

export const DropTitle = styled.div`
font-size: calc(${VariableTheme.font_size.medium} + 0.33vw - 0.66em);
    &:after{
      content:  '${props => props.content}';
    }


`;

////////////////////////////////////// ANI COMPONENT  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const Fade = transition.div`

  &:enter {
    opacity: 0.01;
    transition: opacity 0.2 ease-in;
  }
  &:enter-active {
    opacity: 1;
    transition: opacity 0.2 ease-in;
  }
  &:exit {
    opacity: 1;
    transition: opacity 0.2 ease-in;

  }
  &:exit-active {
    opacity: 0.01;
    transition: opacity 0.2 ease-in;

  }
`;

export const SearchFlex = transition.div`

  &:enter {
   flex-basis: 0%;
  }
  &:enter-active {
    flex-basis: 100%;
  }
  &:exit {
    flex-basis: 100%;
    transition:  flex-basis 0.2 ease-in;

  }
  &:exit-active {
    flex-basis: 100%;
    transition:  flex-basis 0.2 ease-in;

  }
`;

////////////////////////////////////// SPOTLIGHT COMPONENT  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const Spotlight = styled.div`
height: 100%;
width: 100%;
position: fixed;
top: 0;
left: 0;
background: rgba(0,0,0,0.6);
z-index: 99999999;
display: flex;
justify-content: center;
align-items: center;
flex-flow: wrap;

&:after{
  content: '';
  height: 100%;
  width: 100%;
  position: absolute;
  z-index: -1;
  background: hsla(0, 2%, 62%, 0.75);
  -webkit-filter: blur(129px);
  filter: blur(129px);
}
}


`;

export const Model = styled.div`
    height: auto;
    width: 75%;
    margin: 0 auto;
`;

export const Close = styled.span`
    ${props =>
        props.spotlight &&
        css`
            color: white;
            font-size: calc(${VariableTheme.font_size.large} + 0.66vw - 0.5em);
            position: absolute;
            top: 3vh;
            right: 3vw;
            border: 2px solid white;
            height: 50px;
            width: 50px;
            border-radius: 50%;
            text-align: center;
            line-height: 50px;
            cursor: pointer;
        `};
`;

////////////////////////////////////// TABLE COMPONENT  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const Table = styled.table`
    width: 100%;
    margin: 0 auto;
    background-color: white;
    height: auto;
    min-height: 300px;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
    font-family: ${VariableTheme.font_face.fontPrimary};

    @media screen and (max-width: 750px) {
        border: 0;
    }
`;

export const TRow = styled.tr`
    background-color: white;
    border-bottom: 1px solid #ddd;
    padding: 0.5em;

    @media screen and (max-width: 750px) {
        border-bottom: 3px solid #ddd;
        display: block;
        margin-bottom: 0.625em;
    }
`;

export const THeader = styled.th`
    padding: 2em;
    text-align: center;
    font-size: calc(${VariableTheme.font_size.medium} + 0.5vw - 0.66em);
    letter-spacing: 0.1em;
    text-transform: uppercase;
    font-weight: 600;

    @media screen and (max-width: 750px) {
        border: none;
        clip: rect(0 0 0 0);
        height: 1px;
        margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 1px;
    }
`;

export const TCell = styled.td`
    padding: 1.75em;
    text-align: center;
    font-size: calc(${VariableTheme.font_size.medium} + 0.33vw - 0.5em);

    @media screen and (max-width: 750px) {
        border-bottom: 1px solid #ddd;
        display: block;
        text-align: right;
        line-height: 20px;

        &:before {
            content: attr(data-label);
            float: left;
            font-weight: bold;
            text-transform: uppercase;
        }

        &:last-child {
            border-bottom: 0;
        }
    }
`;

////////////////////////////////////// ICONS COMPONENT  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const Icon = styled.span`
    ${props =>
        props.user &&
        css`
            color: black;
            font-size: calc(60px + 0.75vw - 0.25em);
            display: block;
            padding: 10px;

            &:after {
                content: '\f007';
                font-family: FontAwesome;
                font-style: normal;
                font-weight: normal;
                text-decoration: inherit;
            }
        `} ${props =>
        props.search &&
        css`
            color: black;
            font-size: calc(${VariableTheme.font_size.medium} + 0.66vw - 0.25em);
            position: absolute;
            bottom: 40px;
            right: 30px;
            transform: translateY(100%);
            cursor: pointer;

            &:after {
                content: '\f002';
                font-family: FontAwesome;
                font-style: normal;
                font-weight: normal;
                text-decoration: inherit;
            }

            ${props =>
                props.nav &&
                css`
                    opacity: 0;

                    @media (max-width: 800px) {
                        opacity: 1;
                        position: relative;
                        top: 0;
                        right: 0;
                    }
                `};
        `};
`;
