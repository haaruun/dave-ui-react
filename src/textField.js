import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Label, Input, Wrapper, Error } from './theme';

export const TextField = props => {
    const { label, value, type, changeFunc, error, name, ...other } = props;
    return (
        <Wrapper inputWrap>
            <Input {...other} name={name} value={value} type={type} onChange={changeFunc} required />
            <Label>{label}</Label>
            {(error !== null || error !== '') && <Error>{error}</Error>}
        </Wrapper>
    );
};

TextField.propTypes = {
    label: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['text', 'number', 'password', 'email', 'date']).isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    changeFunc: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
};

TextField.defaultProps = {
    error: ''
};
