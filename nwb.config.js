module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'DaveUiReact',
      externals: {
        react: 'React'
      }
    }
  }
}
