import React, {Component} from 'react'
import {render} from 'react-dom'
import { Card, SearchField, CheckField } from '../../src';

const divStyle = {
  backgroundColor: 'red',
  height: '200px',
  padding: '20px'
};

class Demo extends Component {
  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = { checked: true };
  }
  render() {
    return <div>
                   <Card><CheckField
                   id={1} 
                   name={"Hello"} 
                   value={"Sure"} 
                   label={"This is a selected checkbox"} 
                   changeFunc={()=>this.setState({checked: !this.state.checked})} 
                   checked={true} 
                   scale={12}
                   disabled={true}
                   fontSize={12}
                   /></Card>
    </div>
  }
}

render(<Demo/>, document.querySelector('#demo'))
